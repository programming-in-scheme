## About

Adaptation of the `Circle²` program from chapter "Sample Projects II".
Additionally to the original batch version there's an interactive demo
using the SDL2 egg.  Drawing is performed with [Kooda's cairo egg]
using CGA color.

## Interactive controls

| Key             | Function           |
|-----------------+--------------------|
| q, ESC          | Quit               |
| <left>, <right> | Adjust x           |
| <up>, <down>    | Adjust y           |
| <pgup>, <pgdn>  | Adjust sq-length   |
| s               | Screenshot         |
| t               | Toggle status line |

[Kooda's cairo egg]: https://www.upyum.com/cgit.cgi/chicken-cairo/
