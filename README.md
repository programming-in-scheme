## About

CHICKEN Scheme ports of exercises from "Programming in Scheme" (1988),
a beginner-friendly book written by some of the SICP authors.  The
text uses PC Scheme, a Scheme system reminiscent of MIT Scheme.

## Progress

### Chapter 4: Making Choices

### Chapter 5: Recursion

### Chapter 7: Sample Projects II

- [Circle²]

### Chapter 8: Pairs, Lists and Symbols

### Chapter 10: Sample Projects III

### Chapter 12: Procedures

### Chapter 13: Altering Bindings, Altering Projects

### Chapter 15: Sample Projects IV

### Chapter 16: A Sampler of Advanced Topics

[Circle²]: ./circle-squared
